#include <stdio.h>
#include "msg.h"
#include "shell.h"

#include "net/gcoap.h"

#include "periph/gpio.h"
#include "lsm303dlhc.h"

#define ENABLE_DEBUG (0)
#include "debug.h"

#define I2C_INTERFACE              I2C_DEV(0)    /* I2C interface number */

#define MAIN_QUEUE_SIZE       (8)
static msg_t _main_msg_queue[MAIN_QUEUE_SIZE];

#ifndef NODE_LAT
#define NODE_LAT "48.714784"
#endif
#ifndef NODE_LNG
#define NODE_LNG "2.205502"
#endif

static uint8_t response[64] = { 0 };
static lsm303dlhc_t lsm303dlhc_dev;

ssize_t position_handler(coap_pkt_t* pdu, uint8_t *buf, size_t len)
{
    ssize_t p = 0;
    gcoap_resp_init(pdu, buf, len, COAP_CODE_CONTENT);
    p += sprintf((char*)response, "{\"lat\":%s,\"lng\":%s}",
                 NODE_LAT, NODE_LNG);
    response[p] = '\0';
    memcpy(pdu->payload, response, p);

    return gcoap_finish(pdu, p, COAP_FORMAT_TEXT);
}

ssize_t lsm303dlhc_temperature_handler(coap_pkt_t *pdu, uint8_t *buf, size_t len)
{
    gcoap_resp_init(pdu, buf, len, COAP_CODE_CONTENT);
    memset(response, 0, sizeof(response));
    int16_t temperature = 0;
    lsm303dlhc_read_temp(&lsm303dlhc_dev, &temperature);
    sprintf((char*)response, "%i°C", temperature);
    
    size_t payload_len = sizeof(response);
    memcpy(pdu->payload, response, payload_len);

    return gcoap_finish(pdu, payload_len, COAP_FORMAT_TEXT);
}

/* CoAP resources (alphabetical order) */
static const coap_resource_t _resources[] = {
    { "/position", COAP_GET, position_handler },
    { "/temperature", COAP_GET, lsm303dlhc_temperature_handler },
};

static gcoap_listener_t _listener = {
    (coap_resource_t *)&_resources[0],
    sizeof(_resources) / sizeof(_resources[0]),
    NULL
};

static const shell_command_t shell_commands[] = {
    { NULL, NULL, NULL }
};

int main(void)
{
    puts("RIOT IoTLAB A8 M3 Node application");

    /* Initialise the I2C serial interface as master */
    int init = lsm303dlhc_init(&lsm303dlhc_dev, I2C_INTERFACE,
                               GPIO_PIN(PORT_B,1),
                               GPIO_PIN(PORT_B,2),
                               25,
                               LSM303DLHC_ACC_SAMPLE_RATE_10HZ,
                               LSM303DLHC_ACC_SCALE_2G,
                               30,
                               LSM303DLHC_MAG_SAMPLE_RATE_75HZ,
                               LSM303DLHC_MAG_GAIN_400_355_GAUSS);
    if (init == -1) {
        puts("Error: Init: Given device not available\n");
    }
    else {
        printf("Sensor successfuly initialized!");
    }

    /* gnrc which needs a msg queue */
    msg_init_queue(_main_msg_queue, MAIN_QUEUE_SIZE);

    puts("Waiting for address autoconfiguration...");
    xtimer_sleep(3);

    /* start coap server loop */
    gcoap_register_listener(&_listener);

    puts("All up, running the shell now");
    char line_buf[SHELL_DEFAULT_BUFSIZE];
    shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

    return 0;
}
