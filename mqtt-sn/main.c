/*
 * Copyright (C) 2017 Inria
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "xtimer.h"
#include "shell.h"
#include "msg.h"
#include "net/emcute.h"
#include "net/ipv6/addr.h"
#include "board.h"

#include "periph/gpio.h"
#include "lsm303dlhc.h"

#define ENABLE_DEBUG   (0)
#include "debug.h"

#define I2C_INTERFACE              I2C_DEV(0)    /* I2C interface number */

#define MQTT_PORT           (1883U)
#define NUMOFSUBS           (16U)
#define TOPIC_MAXLEN        (64U)

#ifndef GATEWAY_ADDR
#define GATEWAY_ADDR "2001:660:3207:102::4"
#endif

#ifndef GATEWAY_PORT
#define GATEWAY_PORT 1885
#endif

#ifndef NODE_ID
#define NODE_ID "node_id_0"
#endif

static const shell_command_t shell_commands[] = {
    { NULL, NULL, NULL }
};

#define EMCUTE_PRIO           (THREAD_PRIORITY_MAIN - 1)

#define MAIN_QUEUE_SIZE       (8)
static msg_t _main_msg_queue[MAIN_QUEUE_SIZE];
static char stack[THREAD_STACKSIZE_DEFAULT];

#define PUBLISH_QUEUE_SIZE     (8U)
static msg_t _publish_msg_queue[PUBLISH_QUEUE_SIZE];
static char publish_stack[THREAD_STACKSIZE_DEFAULT];

static char payload[64] = {0};
static char topic[64] = { 0 };

static lsm303dlhc_t lsm303dlhc_dev;

static int publish(uint8_t *topic, uint8_t *payload)
{
    emcute_topic_t t;
    unsigned flags = EMCUTE_QOS_1;

    printf("[DEBUG] Publish with topic: %s, data: %s and flags: 0x%02x\n",
           topic, payload, (int)flags);

    t.name = (char*)topic;
    if (emcute_reg(&t) != EMCUTE_OK) {
        printf("[ERROR] Unable to obtain topic %s\n", t.name);
        return 1;
    }

    /* step 2: publish data */
    if (emcute_pub(&t, (char*)payload,
                   strlen((char*)payload), flags) != EMCUTE_OK) {
        printf("[ERROR] Unable to publish data to topic '%s [%i]'\n",
              t.name, (int)t.id);
        return 1;
    }

    printf("[DEBUG] Published %i bytes to topic '%s [%i]'\n",
           (int)strlen((char*)payload), t.name, t.id);

    return 0;
}

static int initialize_mqtt_node(void)
{
    sock_udp_ep_t gw = {.family = AF_INET6, .port = GATEWAY_PORT};

    /* parse address */
    if (ipv6_addr_from_str((ipv6_addr_t *)&gw.addr.ipv6, GATEWAY_ADDR) == NULL) {
        printf("[ERROR] error parsing IPv6 address\n");
        return -1;
    }

    if (emcute_con(&gw, true, NULL, NULL, 0, 0) != EMCUTE_OK) {
        printf("[ERROR] unable to connect to [%s]:%i\n",
              GATEWAY_ADDR, (int)GATEWAY_PORT);
        return -1;
    }
    printf("[INFO] Successfully connected to gateway at [%s]:%i\n",
          GATEWAY_ADDR, (int)GATEWAY_PORT);

    return 0;
}

void *publish_thread(void *args)
{
    (void) args;
    msg_init_queue(_publish_msg_queue, PUBLISH_QUEUE_SIZE);
    for(;;) {
        memset(topic, 0, sizeof(topic));
        sprintf(topic, "node/%s/temperature", NODE_ID);
        memset(payload, 0, sizeof(payload));
        int16_t temperature = 0;
        lsm303dlhc_read_temp(&lsm303dlhc_dev, &temperature);
        sprintf((char*)payload, "%i°C", temperature);
        publish((uint8_t*)topic, (uint8_t*)payload);

        /* wait 5 seconds */
        xtimer_sleep(5);
    }
    return NULL;
}

void init_mqtt_sender(void)
{
    /* create the publish thread that will send periodic measures to
       the broker */
    int publish_pid = thread_create(publish_stack, sizeof(publish_stack),
                                    THREAD_PRIORITY_MAIN - 1,
                                    THREAD_CREATE_STACKTEST, publish_thread,
                                    NULL, "Publish thread");
    if (publish_pid == -EINVAL || publish_pid == -EOVERFLOW) {
        printf("[ERROR] Failed to create bmx280 sender thread, exiting\n");
    }
    else {
        printf("[ERROR] Successfuly created beaconing thread !\n");
    }
}

static void *emcute_thread(void *arg)
{
    (void)arg;
    emcute_run(MQTT_PORT, NODE_ID);
    return NULL;    /* should never be reached */
}

/* import "ifconfig" shell command, used for printing addresses */
extern int _netif_config(int argc, char **argv);

int main(void)
{
    puts("RIOT MQTT-SN application");

    /* Initialise the I2C serial interface as master */
    int init = lsm303dlhc_init(&lsm303dlhc_dev, I2C_INTERFACE,
                               GPIO_PIN(PORT_B,1),
                               GPIO_PIN(PORT_B,2),
                               25,
                               LSM303DLHC_ACC_SAMPLE_RATE_10HZ,
                               LSM303DLHC_ACC_SCALE_2G,
                               30,
                               LSM303DLHC_MAG_SAMPLE_RATE_75HZ,
                               LSM303DLHC_MAG_GAIN_400_355_GAUSS);
    if (init == -1) {
        puts("Error: Init: Given device not available\n");
    }
    else {
        printf("Sensor successfuly initialized!");
    }

    /* gnrc which needs a msg queue */
    msg_init_queue(_main_msg_queue, MAIN_QUEUE_SIZE);

    puts("Waiting for address autoconfiguration...");
    xtimer_sleep(3);

    /* start the emcute thread */
    thread_create(stack, sizeof(stack), EMCUTE_PRIO, 0,
                  emcute_thread, NULL, "emcute");

    if (initialize_mqtt_node() < 0) {
        puts("Failed to initialize MQTT node");
    }

    init_mqtt_sender();

    puts("All up, running the shell now");
    char line_buf[SHELL_DEFAULT_BUFSIZE];
    shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

    return 0;
}
